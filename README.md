The LREMP stack

Linux (as if Windows was on option) ReactJS NGINX MySQL PHP

There are a bunch of binaries which all read stuff from this big __.env__ file
You should edit your own .env file, start from .evn.dist

Main directories are:
- **docker** directory contains all important docker related files such as Dockerfiles, docker-compose-yml, configs, database data and other stuff
- **bin** containes all binaries, manipulation docker for the most part
- **app** should contain only aplication specific files, code, configs, web public directory etc.

Docker:
- to handle file permissions, some images are given new users with preferably the same ID as your user has. Usually it will be 1000. This way when filesystem actions are taking place inside containers (php creating files, compiling javsacript and css), locally **YOU** own those files
- node based images already have an user with id 1000
- php is configured to run as certain user in **zz-docker.conf**

DNS:
- there is also an image dedicated to handle DNS queries (due to possible issues with DNS resolving from local host)
- hosts may be configured inside hosts file (see volumes for this image in docker-compose.yml)

Binaries:
- to execute commands I recommend you to change to **/bin** directory
- all commands are being executed through this sort of **bootstrap** executable **control**
- for example, executing **build** script inside **/bin/docker** you type  this: **./control docker/build**
- when changed to **/bin** directory, you get the nice autocompletion feature, so thats why

Commands:

**docker/build**

Builds all docker images defined in $SERVICES variable

**docker/clear**

Stops and removes all containers and also images defined by $SERVICES variable

**docker/rebuild**

Clears and then builds (see previous commands)

**docker/fireup** [stack] ?[-d]

Fires up al containers by running **docker-compose up** with passed file found in **/docker/stacks/[stack]** (for example **dev**)

You can also pass **-d** option to daemonize it

**docker/teardown** [stack]

Shuts down all running containers defined in appropriate docker-compose.yml file, again, use **stack** argument to specifiy the file

**docker/shellin** [service-name]

Shells you into container associated with given service which is one of those in $SERVICES

UI TOOLS:
- ui image has sass and webpack installed
- use scripts from package.json to compile css and javascript