module.exports = env => {
    return {
        mode: 'development',
        entry: './src/main.js',
        output: {
          path: env.JS_PUBLIC_DIR
        },
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        "presets": ["@babel/preset-env", "@babel/preset-react"]
                    }
                }
            }]
        }
    }
};